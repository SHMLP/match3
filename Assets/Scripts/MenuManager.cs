using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuManager : MonoBehaviour
{
    AudioSource sounds;
    public AudioClip level, exitLevel,exitGame;
    public AudioClip[] startLevelSounds;

    public GameObject levels;

    public static int numeroInicioDeNivel, numeroSiguienteDeNivel;

    public Image fade;
    private void Start()
    {
        //Habilita niveles que se hallan desbloqueado y reproduce musica de nivel
        sounds = Camera.main.GetComponent<AudioSource>();
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("MainMenu"))
        {
            for (int i = 0; i <= numeroSiguienteDeNivel; i++)
            {
                levels.transform.GetChild(i).GetComponent<Button>().interactable = true;
            }
        }
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level"))
            sounds.PlayOneShot(startLevelSounds[Random.Range(0, startLevelSounds.Length)]);
    }
    public void Levels(int num)
    { 
        //Llama coroutine Clevel
        StartCoroutine(CLevel(num));
    }

    IEnumerator CLevel(int num)
    {
        //Llama a la escena del nivel, reproduce sonido y fade
        sounds.PlayOneShot(level);
        for (float i = 0; i <= 1; i = i + 0.05f)
        {
            fade.color = new Color(1, 1, 1, i);
            yield return null;
        }
        fade.color = new Color(1, 1, 1, 1);
        numeroInicioDeNivel = num;
        SceneManager.LoadScene("Level");

    }

    public void Exit()
    {
        //Llama coroutine CExit
        StartCoroutine(CExit());
    }

    IEnumerator CExit()
    {
        //Sale del juego o del nivel segun sea el caso, reproduce sonido y fade
        for (float i = 0; i <= 1; i = i + 0.01f)
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level"))
            {
                fade.color = new Color(1, 1, 1, i);
                if (i==0)
                    sounds.PlayOneShot(exitLevel);
                yield return null;
            }
            else
            {
                fade.color = new Color(0, 0, 0, i);
                if (i==0)
                    sounds.PlayOneShot(exitGame);
                yield return null;
            }
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level"))
            SceneManager.LoadScene("MainMenu");
        else
            Application.Quit();
    }

}
