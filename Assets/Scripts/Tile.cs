using System.Collections;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public int indiceX, indiceY;
    Board board;



    public void Ini(int i, int j, Board _board)
    {
        //Se inicializa el tile
        indiceX = i;
        indiceY = j;
        board = _board;
    }

    private void OnMouseDown()
    {
        //Le entrega  a la funcion selectTile su propio tile
        board.SelectTile(this,null);
    }

    private void OnMouseEnter()
    {
        //Le entrega  a la funcion selectTile su propio tile
        board.SelectTile(null, this);
    }

    private void OnMouseUp()
    {
        //LLama la funcion DeselectTile para mover Piezas y Rinicial los tiles seleccionados
        board.DeselectTile();
    }

    public IEnumerator Color(Color color)
    {
        //Reproduce la animacion del Tile
        GetComponent<SpriteRenderer>().color = color;
        GetComponent<Animator>().SetTrigger("start");
        yield return new WaitForSeconds(1f);
        GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
    }

}
