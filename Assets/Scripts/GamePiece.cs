using System.Collections;
using UnityEngine;

public class GamePiece : MonoBehaviour
{
    public int indexX, indexY,number;

    Board board;

    public Color color;
    //public delegate void asda();
    //public static asda a;

    public TipoMovimiento tipoMovimiento;

    public void Ini(int x, int y, Board _board)
    {
        //Inicializa gamepiece
        indexX = x;
        indexY = y;
        board = _board;
    }

    public void Destruir()
    {
        //Destruye gamepiece
        Destroy(gameObject);
    }

    //private void OnEnable()
    //{
    //    a += Prue;
    //}

    //public void Prue()
    //{
    //    if (Board.uno == transform.position)
    //    {
    //        StartCoroutine(MoverPieza(Board.uno, Board.dos, 0.25f));
    //    }
    //    if (Board.dos == transform.position)
    //    {
    //        StartCoroutine(MoverPieza(Board.dos, Board.uno, 0.25f));

    //    }
    //}

    public IEnumerator MoverPieza(int x, int y, float deltatiempo)
    {
        //Desplaza la Pieza a una posicion especifica, establece tipo de movimiento y le entrega su nueva posicion
        Vector3 posicionInicial = transform.position;
        Vector3 posicionFinal = new Vector3(x,y,10);
       
        float funcion = 0;
        float time = 0;
        while (Vector3.Distance(transform.position, posicionFinal) > 0.01f)
        {
            switch (tipoMovimiento)
            {
                case TipoMovimiento.lineal:
                    funcion = time / deltatiempo;
                    break;
                case TipoMovimiento.sin:
                    funcion =  Mathf.Sin(time / deltatiempo*Mathf.PI*0.5f);
                    break;
                case TipoMovimiento.cos:
                    funcion =  1 - Mathf.Cos(time / deltatiempo * Mathf.PI * 0.5f);
                    break;
                case TipoMovimiento.suave:
                    funcion =  Mathf.Pow(time, 2) * (3 - 2 * time);
                    break;
                case TipoMovimiento.masuave:
                    funcion = Mathf.Pow(time, 2) *( time*(time *6-15)+10);
                    break;
            }
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, funcion);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = Vector3.Lerp(posicionInicial, posicionFinal, 1);
        indexX = x;
        indexY = y;
    }

  

    private void OnMouseDown()
    {

        //if (!Board.primerClick)
        //{
        //    Board.primerClick = true;
        //    Board.uno = transform.position;
        //}
        //else
        //{
        //    Board.primerClick = false;
        //    Board.dos = transform.position;
        //    a.Invoke();
        //}
 

    }

    public enum TipoMovimiento
    {
        //enum Tipo de movimientos
        lineal,
        sin,
        cos,
        suave,
        masuave
    }
}
