using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Board : MonoBehaviour
{
    //public static GameObject[,] arrayContenerdor;

    //public Sprite[] sprites;
    //public Color[] colors;

    //public static bool primerClick;
    //public static Vector3 uno, dos;
  
    public int width, height, modalidad, puntajeLimite;

    int puntaje = 0;
    public bool onMovement;

    
    AudioSource sounds;
    public AudioClip[] combosSounds;
    public AudioClip loseSound, winSound;


    public TextMeshProUGUI score, combo, cronometroMin, cronometroSeg, movimientos, gameOver, conseguirPuntaje, faltante;

    public GameObject prefab;

    public GameObject[] pieces;

    public GamePiece[,] gamePieces;
    public Tile[,] board;

    public Tile primerTileSelecionado;
    public Tile segundoTileSelecionado;

    List<GamePiece>[] sumaUno = new List<GamePiece>[2];
    List<GamePiece>[] sumaDos = new List<GamePiece>[2];

    void Start()
    {
        InicioDeNivel(MenuManager.numeroInicioDeNivel);

        sounds = Camera.main.GetComponent<AudioSource>();

        width = width < 3 ? 3 : width;
        height = height < 3 ? 3 : height;

        float ratio = Screen.width / (float)Screen.height;
        float num = Mathf.Max((width / 2f) / ratio + 0.5f, (height / 2f) + 0.5f);
        GetComponent<Camera>().orthographicSize = num;

        board = new Tile[width, height];
        gamePieces = new GamePiece[width, height];

        transform.position = new Vector3((width / 2f) - 0.5f, (height / 2f) - 0.5f, -10);
        CrearTablero(true);
        RectificarTablero();
        BuscarMatchEnTableroRectificado();
        //Establece la modalidad de juego, si es tiempo o por movimientos
        if (modalidad == 0)
        {
            faltante.text = "Tiempo Faltante";
            cronometroMin.gameObject.SetActive(true);
            StartCoroutine(Cronometro());
        }
        else
        {
            faltante.text = "Movimientos Faltantes";
            movimientos.gameObject.SetActive(true);
        }
     
    }
    
    public void InicioDeNivel(int num)
    {
        //Esta funcion establece los valores iniciales para cada nivel del 1 al 5
        width = 6 + num ;
        height = 6 + num;
        modalidad = Random.Range(0, 2);
        puntajeLimite = 100*(num+1);
        movimientos.text = (int.Parse(movimientos.text) + 4 - num).ToString();
        int segundos = int.Parse(cronometroSeg.text) + 5 * (4 - num);
        cronometroMin.text = (segundos / 60).ToString(); 
        cronometroSeg.text = (segundos%60).ToString();
        conseguirPuntaje.text = "gets " + puntajeLimite.ToString() + " points";
    }


    public GamePiece CrearPiece(GameObject obj, int i, int j)
    {
        //Clona en una posicion especifica del juego una pieza del array pieces 
        GameObject newPiece = Instantiate(obj, new Vector3(i, j, 0), Quaternion.identity);
        GamePiece gamePiece = newPiece.GetComponent<GamePiece>();
        newPiece.transform.parent = transform;
        gamePiece.Ini(i, j,this);
        return gamePiece;
    }
    void CrearTablero(bool crearTiles)
    {
        //Crea el tablero inical de juego incluye tile y gamepiece
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (crearTiles)
                {
                    GameObject newObj = Instantiate(prefab, new Vector3(i, j, 0), Quaternion.identity);
                    newObj.name = $"Tile {i} , {j}";
                    newObj.transform.parent = transform;
                    Tile tile = newObj.GetComponent<Tile>();
                    tile.Ini(i, j, this);
                    board[i, j] = tile;
                }
                gamePieces[i,j] = CrearPiece(pieces[Random.Range(0, pieces.Length)], i, j);
            }
        }
    }
    void LimpiarTablero()
    {
        //Elimina todos los gamepiece de la escena
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                gamePieces[i, j].Destruir();
            }
        }
    }
    void RectificarTablero()
    {
        //Busca en todo el tablero inicial posibles matchs, destruye la pieza y coloca una pieza que sea diferente a la que tenga en sus cuatro direcciones
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                int verificar = MatchEnTableroInicial(gamePieces[i, j]);
                if (verificar != gamePieces[i, j].number)
                {
                    gamePieces[i, j].Destruir();
                    gamePieces[i, j] = CrearPiece(pieces[verificar-1], i, j);
                }
            }
        }
    }
    void BuscarMatchEnTableroRectificado()
    {
        //Asegura la existencia de al menos un mactch para cuando empiece el juego
        bool match = false;
        while (!match)
        {
            match = EncontrarPosibleMatch();
            if (!match)
            {
                LimpiarTablero();
                CrearTablero(false);
                RectificarTablero();
            }
        }
    }




    void ColumnasParaTrasladarPiezas(List<List<GamePiece>[]> match)
    {
        //Busca todas las columnas implicadas en un match, si hay, las traslada, si no, deja al usuario mover las piezas y verifica si el juego termino
        List<int> columnas = new List<int>();
        for (int i = 0; i < match.Count; i++)
        {
            for (int j = 0; j < match[i].Length; j++)
            {
                if (match[i][j].Count >= 3)
                {
                    foreach (GamePiece item in match[i][j])
                    {
                        if (!columnas.Contains(item.indexX))
                            columnas.Add(item.indexX);
                    }
                }
            }
        }
        if (columnas.Count > 0)
            TrasladarPiezasAfterMatch(columnas);
        else
        {
            onMovement = false;
            bool verficarPosibleMatch = EncontrarPosibleMatch();
            if (!verficarPosibleMatch || movimientos.text == "0" || (cronometroMin.text == "0" && cronometroSeg.text == "0"))
                StartCoroutine(GameOver());
        }
    }
    void TrasladarPiezasAfterMatch(List<int> columnas)
    {
        //traslada las columnas hacia abajo si hay match y crea nuevas piezas en estas columnas
        List<List<GamePiece>> pieces = new List<List<GamePiece>>();
        List<List<Tile>> vacios = new List<List<Tile>>();
        foreach (int item in columnas)
        {
            vacios.Add(new List<Tile>());
            pieces.Add(new List<GamePiece>());
        }

        for (int i = 0; i < columnas.Count; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (gamePieces[columnas[i], j] == null || vacios[i].Count > 0)
                    vacios[i].Add(board[columnas[i], j]);
                if ((j>=1 && gamePieces[columnas[i], j] != null && gamePieces[columnas[i], j - 1] == null) || (pieces[i].Count > 0 && gamePieces[columnas[i], j] != null))
                    pieces[i].Add(gamePieces[columnas[i],j]);

            }
        }

        for (int i = 0; i < columnas.Count; i++)
        {
            int hasta = Mathf.Abs(vacios[i].Count - pieces[i].Count);
            for (int j = 0; j < hasta; j++)
            {
                pieces[i].Add(CrearPiece(this.pieces[Random.Range(0, this.pieces.Length)], columnas[i], vacios[i][j].indiceY + height - vacios[i][0].indiceY));
            }
        }

        for (int i = 0; i < columnas.Count; i++)
        {
            for (int j = 0; j < vacios[i].Count; j++)
            {
                gamePieces[vacios[i][j].indiceX, vacios[i][j].indiceY] = pieces[i][j];
                StartCoroutine(pieces[i][j].MoverPieza(vacios[i][j].indiceX, vacios[i][j].indiceY, 0.25f));
            }
        }
        StartCoroutine(VerificarMatchDespuesDeTrasladarPieza(columnas, vacios, pieces));
    }
    IEnumerator VerificarMatchDespuesDeTrasladarPieza(List<int> columnas, List<List<Tile>> vacios, List<List<GamePiece>> pieces)
    {
        //Verifica si hay matchs en las columnas que se trasladaron hacia abajo, vuelve a llamar a columnasparatrasladarpiezas y asigna un puntaje
        yield return new WaitForSeconds(0.5f);
        int numeroDeMatchParaCombo = 1;
        List<List<GamePiece>[]> listaDeMatchs = new List<List<GamePiece>[]>();
        for (int i = 0; i < columnas.Count; i++)
        {
            for (int j = vacios[i][0].indiceY; j < height; j++)
            {
                if (gamePieces[columnas[i], j] != null)
                {
                    sumaUno = Match(gamePieces[columnas[i], j]);
                    if (sumaUno[0].Count >= 3 || sumaUno[1].Count >= 3)
                    {
                        listaDeMatchs.Add(sumaUno);
                        yield return StartCoroutine(PintarTileMatch(sumaUno));
                        sounds.PlayOneShot(combosSounds[Random.Range(0, combosSounds.Length)]);
                        numeroDeMatchParaCombo++;
                    }

                }
            }
        }

        ColumnasParaTrasladarPiezas(listaDeMatchs);
        if (numeroDeMatchParaCombo>1)
        {
            score.text =  (int.Parse(score.text) + puntaje * Mathf.Clamp(numeroDeMatchParaCombo,2,9)).ToString();
            combo.text = "X" + numeroDeMatchParaCombo.ToString();
            yield return new WaitForSeconds(1f);
            combo.text = "";
        }
    }


    public void SelectTile(Tile primerTile, Tile segundoTile)
    {
        //Asigna los tiles que posteriormente se usaran para asignar los gamepiece que se moveran
        if (!onMovement)
        {
            if (primerTile!=null)
            {
                if (primerTileSelecionado == null)
                    primerTileSelecionado = primerTile;
            }
            else
            {
                if (primerTileSelecionado != null)
                {
                    if (movimientos.text != "0" && (cronometroMin.text != "0" || cronometroSeg.text != "0"))
                    {
                        if (modalidad == 1)
                            movimientos.text = (int.Parse(movimientos.text) - 1).ToString();
                        if ((primerTileSelecionado.indiceY == segundoTile.indiceY && Mathf.Abs(segundoTile.indiceX - primerTileSelecionado.indiceX) == 1) || (primerTileSelecionado.indiceX == segundoTile.indiceX && Mathf.Abs(segundoTile.indiceY - primerTileSelecionado.indiceY) == 1))
                            segundoTileSelecionado = segundoTile;
                    }
                }
            }
        }
        
    }
    public void DeselectTile()
    {
        //verifica que hallan tiles asignados y llama a switchtile
        if (primerTileSelecionado != null && segundoTileSelecionado != null && !onMovement)
        {
            onMovement = true;
            SwitchTile(); 
        }
    }
    public void SwitchTile()
    {
        //Intercambia los gamepiece de posicion

        GamePiece a = gamePieces[primerTileSelecionado.indiceX, primerTileSelecionado.indiceY];
        GamePiece b = gamePieces[segundoTileSelecionado.indiceX, segundoTileSelecionado.indiceY];

        StartCoroutine(a.MoverPieza(b.indexX, b.indexY, 0.25f));
        StartCoroutine(b.MoverPieza(a.indexX, a.indexY, 0.25f));

        gamePieces[primerTileSelecionado.indiceX, primerTileSelecionado.indiceY] = b;
        gamePieces[segundoTileSelecionado.indiceX, segundoTileSelecionado.indiceY] = a;

        StartCoroutine(ReSwitchPieces(a,b));
    }
    IEnumerator ReSwitchPieces(GamePiece ini, GamePiece fin)
    {
        //verifica si hay match, si hay, pinta los macht, asigna puntajes y llama a ColumnasParaTrasladarPiezas; si no, intercambia de nuevo las posiciones de los gamepiece
        yield return new WaitForSeconds(0.5f);
        sumaUno = Match(ini);
        sumaDos = Match(fin);
        if (sumaUno[0].Count >= 3 || sumaUno[1].Count >= 3 || sumaDos[0].Count >= 3 || sumaDos[1].Count >= 3)
        {

            yield return StartCoroutine(PintarTileMatch(sumaUno));
            yield return StartCoroutine(PintarTileMatch(sumaDos));
            bool dos = false;
            List<List<GamePiece>[]> listaDeMatchs = new List<List<GamePiece>[]>();
            if ((sumaUno[0].Count >= 3 || sumaUno[1].Count >= 3) && (sumaDos[0].Count >= 3 || sumaDos[1].Count >= 3))
            {
                Score(sumaUno,sumaDos,2);
                dos = true;
            }
            if (sumaUno[0].Count >= 3 || sumaUno[1].Count >= 3)
            {
                listaDeMatchs.Add(sumaUno);
                if (!dos)
                    Score(sumaUno);
            }
            if (sumaDos[0].Count >= 3 || sumaDos[1].Count >= 3)
            {
                listaDeMatchs.Add(sumaDos);
                if (!dos)
                    Score(sumaDos);
            }

            ColumnasParaTrasladarPiezas(listaDeMatchs);

            primerTileSelecionado = null;
            segundoTileSelecionado = null;

            
        }
        else
        {
            StartCoroutine(ini.MoverPieza(fin.indexX, fin.indexY, 0.25f));
            StartCoroutine(fin.MoverPieza(ini.indexX, ini.indexY, 0.25f));

            gamePieces[primerTileSelecionado.indiceX, primerTileSelecionado.indiceY] = ini;
            gamePieces[segundoTileSelecionado.indiceX, segundoTileSelecionado.indiceY] = fin;

            primerTileSelecionado = null;
            segundoTileSelecionado = null;

            onMovement = false;
        }

    }



    IEnumerator PintarTileMatch(List<GamePiece>[] tiles)
    {
        //Reproduce la animacion de los tiles donde hay match y destruye las piezas
        List<GamePiece> listaJunta = new List<GamePiece>();
        foreach (List<GamePiece> item in tiles)
        {
            foreach (GamePiece gameP in item)
            {
                if (!listaJunta.Contains(gameP) )
                    listaJunta.Add(gameP);
            }      
        }
        foreach (GamePiece item in listaJunta)
        {
            StartCoroutine(board[item.indexX, item.indexY].Color(item.color));
        }
        yield return new WaitForSeconds(0.5f);
        foreach (GamePiece item in listaJunta)
        {
            item.Destruir();
            gamePieces[item.indexX, item.indexY] = null;
        }
        yield return new WaitForSeconds(0.5f);
    }
    List<GamePiece>[] Match(GamePiece inicial)
    {
        //Busca un match en las piezas que movio el usuario 
        List<GamePiece>[] suma = new List<GamePiece>[2];
        suma[0] = new List<GamePiece>();
        suma[1] = new List<GamePiece>();
        suma[0].Add(inicial);
        suma[1].Add(inicial);
        for (int j = 0; j <= 270; j = j + 90)
        {
            Vector2 hacia = new Vector2(Mathf.Cos(Mathf.Deg2Rad * j), Mathf.Sin(Mathf.Deg2Rad * j));
            for (int k=1; ; k++)
            {
                int x = inicial.indexX + k * (int)hacia.x;
                int y = inicial.indexY + k * (int)hacia.y; 
                try
                {
                    if (inicial.number == gamePieces[x,y].number)
                    {                          
                        if ((int)hacia.x == 0)
                            suma[0].Add(gamePieces[x, y]);
                        else
                            suma[1].Add(gamePieces[x, y]);
                    }
                    else
                        break;
                }
                catch (System.Exception)
                {
                    break;
                }
            }
        }
        if (suma[0].Count<3)
            suma[0].Clear();
        if (suma[1].Count<3)
            suma[1].Clear();
        return suma;
    }
    int MatchEnTableroInicial(GamePiece ini)
    {
        //Busca en las cuatro direcciones de ini dos piezas consecutivas identicas y retorna un valor de pieza que sea diferente a estas piezas
        int[] identico = new int[4];
        int random = ini.number;
        for (int j = 0; j <= 270; j = j + 90)
        {
            Vector2 hacia = new Vector2(Mathf.Cos(Mathf.Deg2Rad * j), Mathf.Sin(Mathf.Deg2Rad * j));
            int k = 1;
            int x = ini.indexX + k * (int)hacia.x;
            int y = ini.indexY + k * (int)hacia.y;
            if (x + (int)hacia.x >= 0 && x + (int)hacia.x < width && y + (int)hacia.y >= 0 && y + (int)hacia.y < height)
            {
                if (gamePieces[x, y].number == gamePieces[x + (int)hacia.x, y + (int)hacia.y].number)
                    identico[j / 90] = gamePieces[x, y].number;
            }

        }
        for (int i = 0; i < identico.Length; i++)
        {
            if (ini.number == identico[i])
            {
                random = Random.Range(1, pieces.Length + 1);
                int k = 0;
                while (k < identico.Length)
                {
                    if (identico[k] == random)
                    {
                        random = Random.Range(1, pieces.Length + 1);
                        k = 0;
                    }
                    else
                        k++;
                }
                break;
            }
        }
        return random;
    }
    bool EncontrarPosibleMatch()
    {
        //Busca posibles matchs en la matriz de gamepieces y retorna true si lo encuentra
        List<GamePiece>[] suma = new List<GamePiece>[2];
        suma[0] = new List<GamePiece>();
        suma[1] = new List<GamePiece>();
        bool verficarPosibleMatch = false;
        for (int m= 0; m < width; m++)
        {
            for (int l = 0; l < height; l++)
            {

                GamePiece inicial = gamePieces[m,l];
                for (int i = 0; i <= 270; i = i + 90)
                {
                    Vector2 primerHacia = new Vector2((int)Mathf.Cos(Mathf.Deg2Rad * i), (int)Mathf.Sin(Mathf.Deg2Rad * i));
                    Vector2 final = new Vector2(inicial.indexX + primerHacia.x, inicial.indexY + primerHacia.y);
                    if (final.x >= 0 && final.x < width && final.y >= 0 && final.y < height)
                    {
                        Vector2 verificarDir = new Vector2(inicial.indexX - final.x, inicial.indexY - final.y);
                        for (int j = 0; j <= 270; j = j + 90)
                        {

                            Vector2 SegundoHacia = new Vector2(Mathf.Cos(Mathf.Deg2Rad * j), Mathf.Sin(Mathf.Deg2Rad * j));
                            for (int k = 1; ; k++)
                            {

                                int x = (int)final.x + k * (int)SegundoHacia.x;
                                int y = (int)final.y + k * (int)SegundoHacia.y;
                                try
                                {
                                    if (verificarDir != SegundoHacia)
                                    {
                                        if (gamePieces[inicial.indexX, inicial.indexY].number == gamePieces[x, y].number)
                                        {
                                            if ((int)SegundoHacia.x == 0)
                                                suma[0].Add(gamePieces[x, y]);
                                            else
                                                suma[1].Add(gamePieces[x, y]);
                                        }
                                        else
                                            break;
                                    }
                                    else
                                        break;
                                }
                                catch (System.Exception)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    if (suma[0].Count >= 2 || suma[1].Count >= 2)
                        break;
                    else
                    {
                        suma[0].Clear();
                        suma[1].Clear();
                    }
                }

                if (suma[0].Count >= 2 || suma[1].Count >= 2)
                {
                    verficarPosibleMatch = true;
                    break;
                }
            }
            if (verficarPosibleMatch)
                break;
        }

        return verficarPosibleMatch;
    }


    void Score(List<GamePiece>[] matchUno, List<GamePiece>[] matchDos = null , int num = 1)
    {
        //Asigna un puntaje dependiendo de la cantidad de piezas en el match y de la forma del match, sea en "L" o "T", tambien reproduce el sonido del match
        List<GamePiece>[] matchs = new List<GamePiece>[2];
        puntaje = 0;
        for (int i = 0; i < num; i++)
        {
            if (i == 0)
                matchs = matchUno;
            else
                matchs = matchDos;

            puntaje = puntaje + 10;
            if (matchs[0].Count == 4 || matchs[1].Count == 4)
                    puntaje = puntaje + 1;
            if (matchs[0].Count >= 5 || matchs[1].Count >= 5)
                puntaje = puntaje + 2;


            bool ele = true;
            bool te = false;
            for (int it = 0; it < matchs.Length; it++)
            {
                if (matchs[0].Count<3 || matchs[1].Count < 3)
                {
                    ele = false;
                    break;
                }
                for (int jt = 0; jt < matchs[it].Count; jt++)
                {
                    if (jt>0 && Mathf.Sign((matchs[it][jt].indexX + matchs[it][jt].indexY) - (matchs[it][0].indexX + matchs[it][0].indexY)) != Mathf.Sign((matchs[it][1].indexX + matchs[it][1].indexY) - (matchs[it][0].indexX + matchs[it][0].indexY)))
                    {
                        ele = false;
                        te = true;
                        break;
                    }
                }
                if (!ele)
                    break;
            }
            if (ele && te)
                puntaje = puntaje + 10;
            if (te)
                puntaje = puntaje + 15;
            sounds.PlayOneShot(combosSounds[Random.Range(0, combosSounds.Length)]);
        }
        score.text = (int.Parse(score.text) + puntaje).ToString();
    }

    IEnumerator Cronometro()
    {
        //Cronometro de la modalidad de tiempo, tambien verifica si el tiempo ya termino
        yield return new WaitForSeconds(1);
        if (cronometroSeg.text == "0")
        {
            if (cronometroMin.text == "0")
                cronometroSeg.text = "0";
            else
            {
                cronometroSeg.text = "60";
                cronometroMin.text = (int.Parse(cronometroMin.text) - 1).ToString();
            }
        }
        if (cronometroSeg.text != "0" || cronometroMin.text != "0")
        {
            cronometroSeg.text = (int.Parse(cronometroSeg.text) - 1).ToString();
            StartCoroutine(Cronometro());
        }
        else
        {
            if (!onMovement)
                StartCoroutine(GameOver());
            else
                StartCoroutine(Cronometro());
        }
    }

    IEnumerator GameOver()
    {
        //Reproduce escena, sonidos y textos segun el caso, sea que el usuario gano o perdio
        gameOver.gameObject.SetActive(true);
        if (int.Parse(score.text) >= puntajeLimite)
        {
            sounds.PlayOneShot(winSound);
            gameOver.text = "You Won";
            if (MenuManager.numeroInicioDeNivel < 4)
                MenuManager.numeroSiguienteDeNivel = MenuManager.numeroInicioDeNivel + 1;
            yield return new WaitForSeconds(4);
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            sounds.PlayOneShot(loseSound);
            gameOver.text = "You Lost";
            yield return new WaitForSeconds(4);
            SceneManager.LoadScene("Level");
        }
    }
}
